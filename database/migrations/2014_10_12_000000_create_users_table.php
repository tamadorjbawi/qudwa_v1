<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username');
            $table->integer('phone');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->default(Hash::make("00000000"));

            $table->enum('role', ['admin', 'superadmin','friend','children'])->default('children');

            $table->rememberToken();
            $table->timestamps();
        });


        \App\Models\User::create([
            'name' => 'superadmin',
            'username' => 'superadmin',
            'role' => 'superadmin',
            "phone"=>0,
             'email' => 'superadmin@managentmosque.com',
             'password' => Hash::make("superadmin2023")

         ]);

         \App\Models\User::create([
            'name' => 'admin',
            'username' => 'admin',
            'role' => 'admin',
            "phone"=>0,
             'email' => 'admin@managentmosque.com',
             'password' => Hash::make("admin2023")

         ]);


         \App\Models\User::create([
            'name' => 'children',
            'username' => 'children',
            'role' => 'children',
            "phone"=>12345678,
             'email' => 'children@managentmosque.com',
             'password' => Hash::make("children")

         ]);

         \App\Models\User::create([
            'name' => 'teacher',
            'username' => 'teacher',
            'role' => 'teacher',
            "phone"=>0,
             'email' => 'teacher@managentmosque.com',
             'password' => Hash::make("teacher2023")

         ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
